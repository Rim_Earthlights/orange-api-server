import { Module } from '@nestjs/common';
import { AppService } from '../app.service';
import { ChatController } from 'src/controller/chat/chat.controller';

@Module({
  imports: [],
  controllers: [ChatController],
  providers: [AppService],
})
export class AppModule {}
