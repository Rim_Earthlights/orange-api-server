import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from '../../app.service';

@Controller()
export class GuildController {
  constructor(private readonly appService: AppService) {}

  @Get('/guild')
  getGuild(@Param() id: string): string {
    return id;
  }
}
