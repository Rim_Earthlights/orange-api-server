import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { AppService } from '../../app.service';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/common/guards/auth/auth.guard';

@Controller('/chat')
@ApiTags('chat')
@UseGuards(AuthGuard)
export class ChatController {
  constructor(private readonly appService: AppService) {}

  @Get('/history/:id')
  history(@Param() id: string): string {
    return id;
  }
}
