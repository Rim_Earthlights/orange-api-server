import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/config/config';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    console.log(request.headers);
    return validateRequest(request);
  }
}

function validateRequest(request: Request): boolean {
  return request.headers['authorization'] === CONFIG.COMMON.SECRET;
}
