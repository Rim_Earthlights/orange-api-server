export type LogData = {
  guild_id?: string;
  channel_id?: string;
  user_id?: string;
  level: LogLevel;
  event: string;
  message?: string[];
};

export enum LogLevel {
  INFO = 'info',
  ERROR = 'error',
  SYSTEM = 'system',
}

/**
 * プレイリストの動画情報
 */
export interface YoutubePlaylists {
  videoId: string;
  title: string;
  thumbnail: string;
}

/**
 * 有効化する機能
 */
export enum functionNames {
  GPT = 'gpt',
  GPT_WITHOUT_KEY = 'gpt_without_key',
  FORECAST = 'forecast',
  YOUTUBE = 'youtube',
}

interface functions {
  name: functionNames;
  enable: boolean;
}
export const ENABLE_FUNCTION: functions[] = [
  { name: functionNames.FORECAST, enable: false },
  { name: functionNames.GPT, enable: false },
  { name: functionNames.GPT_WITHOUT_KEY, enable: false },
  { name: functionNames.YOUTUBE, enable: false },
];

// 連携できるbot
export const COORDINATION_ID = ['985704725016105000'];

export const EXCLUDE_ROOM = ['ロビー', '墓'];

export const ICON = {
  CROWN: ':crown:',
  SPARKLES: ':sparkles:',
  STAR: ':star:',
  STAR2: ':star2:',
  TICKETS: ':tickets:',
  HEART: ':heart:',
};
